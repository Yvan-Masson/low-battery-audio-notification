Simple script that triggers an audio notification when battery is low.

Why?
====

Because when Gnome or KDE triggers a low battery notification, this one is not
displayed on all connected screens which means you can easily miss it. As a
temporary workaround, this script triggers an audio notification when battery is
low.

Corresponding bug reports:

- https://bugs.kde.org/show_bug.cgi?id=471114 and
- https://gitlab.gnome.org/GNOME/gnome-shell/-/issues/6775

How to install
==============

This script should run on any "classical" Linux desktop (it needs systemd,
PulseAudio and Zenity).

One way to install the script:

1. Place the script `low-battery-audio-notification` somewhere, and make it
   executable (`chmod +x low-battery-audio-notification`).
2. Place systemd service unit `low-battery-audio-notification.service` and
   systemd timer unit `low-battery-audio-notification.timer` in
   `~/.config/systemd/user/`.
3. In these two files, adjust values to your needs.
4. Instruct systemd to reload its configuration (`systemctl --user
   daemon-reload`).
